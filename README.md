# [lily58-keymap](https://gitlab.com/eidoom/lily58-keymap)

My personal [lily58 keymap](https://github.com/qmk/qmk_firmware/tree/master/keyboards/lily58/keymaps)

Install using [my fork](https://gitlab.com/eidoom/qmk_firmware/-/blob/master/README-FORK.md)
```shell
sudo dnf install hidapi  # Fedora 34
python3 -m pip install --user qmk
git clone --recurse-submodules git@gitlab.com:eidoom/qmk_firmware.git ~/git/qmk_firmware
qmk setup -H ~/git/qmk_firmware
```
Connect left board.
```shell
qmk flash -kb lily58 -km eidoom -bl dfu-split-left
```
Connect right board.
```shell
qmk flash -kb lily58 -km eidoom -bl dfu-split-right
```
