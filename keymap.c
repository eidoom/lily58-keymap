#include QMK_KEYBOARD_H
#include "transactions.h"

#ifdef CONSOLE_ENABLE
#    include "print.h"
#endif

enum custom_keycodes {
    LF_RST = SAFE_RANGE, // to reseed the life simulation
    LF_SLOW,
    LF_FAST,
    LF_DFLT,
};

enum layer_number {
    _QWERTY,
    _LOWER,
    _RAISE,
    _ADJUST,
    _GME1, // for top-down/isometric perspective games
    _GME2, // for *-person perspective games
};

#define RAISE MO(_RAISE)
#define LOWER MO(_LOWER)
#define ADJUST MO(_ADJUST)
// I tried to set up a 3-way toggle between default and the game modes but it didn't work
// https://docs.qmk.fm/feature_layers#example-keycode-to-cycle-through-layers
#define GME1 TG(_GME1)
#define GME2 TG(_GME2)

const uint16_t PROGMEM keymaps[6][MATRIX_ROWS][MATRIX_COLS] = {
    // clang-format off
[_QWERTY] = LAYOUT( \
  KC_LGUI, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                      KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_GRV,  \
  KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,                      KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSLS, \
  KC_ESC,  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,                      KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT, \
  KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_DEL,  KC_BSPC, KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT, \
                             KC_LALT, KC_LCTL, LOWER,   KC_ENT,  KC_SPC,  RAISE,   KC_RCTL, KC_RALT \
),

[_LOWER] = LAYOUT( \
  _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
  _______, KC_EQL,  KC_PLUS, KC_MINS, KC_UNDS, _______,                   _______, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, _______, \
  _______, _______, _______, _______, _______, _______, GME1   , _______, _______, KC_HOME, KC_PGDN, KC_PGUP, KC_END,  _______, \
                             _______, _______, _______, GME2   , _______, _______, _______, _______ \
),

[_RAISE] = LAYOUT( \
  KC_F12 , KC_F1  , KC_F2  , KC_F3  , KC_F4  , KC_F5  ,                   KC_F6  , KC_F7  , KC_F8  , KC_F9  , KC_F10 , KC_F11 , \
  _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
  _______, KC_LABK, KC_LCBR, KC_LBRC, KC_LPRN, _______,                   _______, KC_RPRN, KC_RBRC, KC_RCBR, KC_RABK, _______, \
  _______, _______, _______, _______, _______, _______, _______, LF_RST , _______, _______, _______, _______, _______, _______, \
                             _______, _______, _______, _______, _______, _______, _______, _______ \
),

[_ADJUST] = LAYOUT( \
  _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
  _______, _______, KC_VOLU, _______, _______, _______,                   _______, _______, LF_FAST, _______, _______, _______, \
  _______, KC_MPRV, KC_VOLD, KC_MPLY, KC_MNXT, KC_BRIU,                   _______, _______, LF_DFLT, _______, _______, _______, \
  _______, _______, KC_MUTE, _______, _______, KC_BRID, _______, _______, _______, _______, LF_SLOW, _______, _______, _______, \
                             _______, _______, _______, _______, _______, _______, _______, _______ \
),

[_GME1  ] = LAYOUT( \
  KC_ENT , _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______, GME1   , _______, _______, _______, _______, _______, _______, _______, \
                             _______, _______, KC_SPC , KC_LCTL, _______, _______, _______, _______ \
),

[_GME2  ] = LAYOUT( \
  KC_ENT , _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
  KC_LSFT, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
  KC_ESC , _______, _______, _______, _______, _______, GME2   , _______, _______, _______, _______, _______, _______, _______, \
                             _______, _______, KC_SPC , KC_LCTL, _______, _______, _______, _______ \
)
    // clang-format on
};

layer_state_t layer_state_set_user(layer_state_t state) {
    return update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST);
}

#ifdef OLED_ENABLE

// see gen_neighbourhoods.py
const uint8_t outcome[512] = {2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 1, 2, 1, 1, 2, 0, 0, 0, 2, 0, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 1, 2, 1, 1, 2, 2, 1, 1, 2, 1, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 0, 0, 0, 2, 2, 2, 1, 2, 1, 1, 2, 2, 1, 1, 2, 1, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 0, 0, 0, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 1, 2, 1, 1, 2, 2, 1, 1, 2, 1, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 0, 0, 0, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                              2, 2, 2, 1, 2, 1, 1, 2, 2, 1, 1, 2, 1, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 0, 0, 0, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// performance bad for F < 4
#    define F 4                          // cell pixel width
#    define W (OLED_DISPLAY_WIDTH / F)   // cells per oled row
#    define H (OLED_DISPLAY_HEIGHT / F)  // cells per oled column
#    define B (8 / F)                    // cells per byte
#    define BH (OLED_DISPLAY_HEIGHT / 8) // bytes per oled column
#    define D 1000                       // default simulation tick time in ms
#    define DD 100                       // step for changing tick time in ms (also min tick time)
#    define SYNC_THROTTLE 500            // throttle delay for sync between sides in ms
#    define RESEED_TIMEOUT 300000        // timeout for reseeding simulation in ms (to avoid burn-in) = 5 minutes

void init_life(void) {
    srand(timer_read());

    // loop byte rows (byte = 8 vertical pixels)
    for (int i = 0; i < BH; ++i) {
        // loop cell columns
        for (int j = 0; j < W; ++j) {
            bool c[B];
            for (int k = 0; k < B; ++k) {
                c[k] = random() % 2;
            }
            // repeat F times for cell width:
            for (int a = 0; a < F; ++a) {
                oled_write_raw_byte(
#    if F == 1
                    0x01 * c[0] +     // 0b00000001
                        0x02 * c[1] + // 0b00000010
                        0x04 * c[2] + // 0b00000100
                        0x08 * c[3] + // 0b00001000
                        0x10 * c[4] + // 0b00010000
                        0x20 * c[5] + // 0b00100000
                        0x40 * c[6] + // 0b01000000
                        0x80 * c[7]   // 0b10000000
#    elif F == 2
                    0x03 * c[0] +     // 0b00000011
                        0x0c * c[1] + // 0b00001100
                        0x30 * c[2] + // 0b00110000
                        0xc0 * c[3]   // 0b11000000
#    elif F == 4
                    0x0f * c[0] +   // 0b00001111
                        0xf0 * c[1] // 0b11110000
#    elif F == 8
                    0xff * c[0] // 0b11111111
#    endif
                    ,
                    OLED_DISPLAY_WIDTH * i + j * F + a);
            }
        }
    }
}

void user_sync_reseed_slave_handler(uint8_t in_buflen, const void* in_data, uint8_t out_buflen, void* out_data) {
    // it's only sent if we want to trigger a reseed so don't actually need the argument data
    init_life();
}

typedef struct _sim_state {
    bool     asleep;
    uint16_t delay;
} sim_state;

sim_state ss = {false, D};

void user_sync_state_slave_handler(uint8_t in_buflen, const void* in_data, uint8_t out_buflen, void* out_data) {
    // only using input data
    const sim_state* rx = (const sim_state*)in_data;
    ss                  = *rx;
}

void keyboard_post_init_user(void) {
    // transaction_register_rpc(USER_SYNC_LIFE, user_sync_life_handler);
    transaction_register_rpc(USER_SYNC_RESEED, user_sync_reseed_slave_handler);
    transaction_register_rpc(USER_SYNC_STATE, user_sync_state_slave_handler);
    init_life();
}

bool reseed_slave = false;

void housekeeping_task_user(void) {
    static uint32_t last_sync = 0;

    if (is_keyboard_master() && timer_elapsed32(last_sync) > SYNC_THROTTLE) {
        if (reseed_slave && transaction_rpc_send(USER_SYNC_RESEED, sizeof(reseed_slave), &reseed_slave)) {
            reseed_slave = false;
        }

        transaction_rpc_send(USER_SYNC_STATE, sizeof(ss), &ss);

        last_sync = timer_read32();
    }
}

uint32_t sleep_timer  = 0;
uint32_t reseed_timer = 0;

bool process_record_user(uint16_t keycode, keyrecord_t* record) {
    sleep_timer = timer_read32();

    switch (keycode) {
        case LF_RST:
            if (record->event.pressed && is_keyboard_master()) {
                init_life();
                reseed_slave = true;
            }
            return false;

        case LF_SLOW:
            if (record->event.pressed && is_keyboard_master()) {
                ss.delay += DD;
            }
            return false;

        case LF_FAST:
            if (record->event.pressed && is_keyboard_master() && ss.delay > DD) {
                ss.delay -= DD;
            }
            return false;

        case LF_DFLT:
            if (record->event.pressed && is_keyboard_master()) {
                ss.delay = D;
            }
            return false;

        default:
            return true;
    }
}

bool prev[H * W];

void pull_prev(void) {
    oled_buffer_reader_t reader = oled_read_raw(0);
    uint8_t              buff_char;

    // byte row index:
    for (int i = 0; i < BH; ++i) {
        // cell column index:
        for (int j = 0; j < W; ++j) {
            buff_char = *reader.current_element;

            for (int k = 0; k < B; ++k) {
                prev[(B * i + k) * W + j] = (buff_char >> (k * F)) & 1;
            }

            reader.current_element += F;
        }
    }
}

void tick_life(void) {
    int last_row[W * (B - 1)];

    for (int i = 0; i < H; ++i) {
        // choose positive modulus for i - 1
        const int im1 = ((i - 1 + H) % H) * W;
        const int ii  = i * W;
        const int ip1 = ((i + 1) % H) * W;

        // start calculating neighbourhood value for lookup table
        // clang-format off
        int neighbourhood =
            0x20 * prev[im1 + H - 1] + 0x04 * prev[im1] +
            0x10 * prev[ii + H - 1]  + 0x02 * prev[ii] +
            0x08 * prev[ip1 + H - 1] + 0x01 * prev[ip1];
        // clang-format on

        for (int j = 0; j < W; ++j) {
            const int jj = (j + 1) % W;

            // reuse overlapping neighbourhood results as iterating over row
            // clang-format off
            neighbourhood = ((neighbourhood & 0x3f) << 3) +
                0x04 * prev[im1 + jj] +
                0x02 * prev[ii + jj] +
                0x01 * prev[ip1 + jj];
            // clang-format on

            // assumes F = 4
            if (i % B) {
                // (F=4) odd rows
                const int top_n = last_row[j], bot_n = outcome[neighbourhood];

                if (top_n == 2 && bot_n == 2) {
                    // no write if no change
                    continue;
                }

                bool top, bot;

                switch (top_n) {
                    case 0:
                        top = false;
                        break;
                    case 1:
                        top = true;
                        break;
                    case 2:
                        top = prev[ii + j - 1];
                        break;
                }

                switch (bot_n) {
                    case 0:
                        bot = false;
                        break;
                    case 1:
                        bot = true;
                        break;
                    case 2:
                        bot = prev[ii + j];
                        break;
                }

                for (int a = 0; a < F; ++a) {
                    oled_write_raw_byte(0x0f * top + 0xf0 * bot, OLED_DISPLAY_WIDTH * ((i - 1) / 2) + j * F + a);
                }

            } else {
                // (F=4) even rows
                // save the outcome so we can write bytes to the oled in the next row iteration
                last_row[j] = outcome[neighbourhood];
            }
        }
    }
}

bool oled_task_user(void) {
    static uint16_t anim_timer = 0;

    if (is_keyboard_master()) {
        if (timer_elapsed32(sleep_timer) > OLED_TIMEOUT) {
            ss.asleep = true;
            oled_off();
            return false;
        } else {
            ss.asleep = false;
        }
        if (timer_elapsed32(reseed_timer) > RESEED_TIMEOUT) {
            init_life();
            reseed_slave = true;
            reseed_timer = timer_read32();
        }
    } else if (ss.asleep) {
        oled_off();
        return false;
    }

    if (timer_elapsed(anim_timer) > ss.delay) {
        anim_timer = timer_read();

        pull_prev();
        tick_life();
    }

    return false;
}

#endif // OLED_ENABLE
