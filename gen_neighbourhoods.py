#!/usr/bin/env python3

from itertools import product

"""
ref: https://codereview.stackexchange.com/a/42790/166931

neighbourhoods are indexed as

8 5 2
7 4 1
6 3 0

and their states represented as a integers
given by reading off the binary number encoding
with right-most least significant bit
with the above indicies

eg

# # _
_ # #
# _ _

=

(1 * 256) + (1 * 32) + (0 * 4) +
(0 * 128) + (1 * 16) + (1 * 2) +
(1 *  64) + (0 *  8) + (0 * 1) = 370

then we construct a length 512 array
with indices corresponding to those numbers
and values of the live/dead outcome
for the centre cell of the neighbourhood
to use as a lookup table
when running the simulation
where

0 = death
1 = birth
2 = unchanged

"""


def state(p):
    c = sum(p)

    if p[4]:
        # S2/3
        return 2 if c in (3, 4) else 0
    else:
        # B3
        return 1 if c == 3 else 2


if __name__ == "__main__":
    print([state(p) for p in product(range(2), repeat=9)])
